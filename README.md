# Document Content #
----

## I BUILD REPLY PROJECT WITH VAGRANT (bellow)
## II [BUILD REPLY PROJECT WITHOUT VAGRANT](https://bitbucket.org/teamreply/reply-readme/src/fde0f26587b627b663c3354374ff0a8a86686043/reply-setup-without-vagrant.md?at=master)


----


# Building Reply Project #


### prerequisites ###

* Vagrant 2.0.0
* VirtualBox 5.1.28
* PHP 7.1
* npm 5.3.0
* node 6.10.2
* composer installed globally ( for windows download installer from https://getcomposer.org/Composer-Setup.exe )

```
#!bash

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
```

### prepare project ###


```
#!bash

mkdir reply-project
cd reply-project
```

* clone reply-test and reply-box repos

```
#!bash

git clone git@bitbucket.org:teamreply/reply-box.git
git clone git@bitbucket.org:teamreply/reply-test.git

```

* create .env file inside reply-test dir and paste the following
```
#!bash

APP_NAME=Reply
APP_ENV=local
APP_KEY=base64:aDu7ZQMABE+4ET4eOdYb4g++OqKbyGsjCMaaFQbcsvo=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=150.10.10.10
DB_PORT=3306
DB_DATABASE=replytest
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
```

* update vagrant box

```
#!bash

cd ../reply-box
vagrant box update
```

* provision vagrant virtual machine

```
#!bash

vagrant up
```

### post install ###

* install front-end dependencies

```
#!bash

cd ../reply-test
npm install
```

* build assets

```
#!bash

npm run dev
```

* login into vm from reply-box directory
```
#!bash

vagrant ssh
```

* install back-end dependencies from reply-test directory

```
#!bash

cd /opt/code/reply-project/reply-api
composer install
```

### testing ###

* modify hosts file locally

```
#!bash

sudo vim /etc/hosts
150.10.10.10 reply.local
```

* (pro tip) use watcher for building assets automatically
```
#!bash

npm run watch
```


* navigate to http://reply-test.local
* you should be able to see something like this:

![Scheme](img/reply-login.png)

* login with:
username: admin@admin.com
password: secret

### git workflow ###

[How to configure SSH access](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html#SetupanSSHkey-ssh1) 


* set user and email for the project

```
#!bash

git config --global user.name "<first_name last_name>"
git config --global user.email "<email>"
```

### important! ###

* never work in master branch
* always checkout feature branch off of fresh master

```
#!bash

git checkout master
git pull
git checkout branch <name of the feature branch>
```

