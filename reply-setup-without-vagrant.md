# How to setup Reply environment locally without Vagrant



>Requirements:


- PHP 7.1.11 _(modules: php-mysql php-fpm php-mbstring php-mcrypt php-xml php-xdebug php-curl)_

- MySQL 5.7.2

- Composer

- Git

- npm

- node

____


## Linux steps

- [How to install/update PHP?](https://askubuntu.com/a/856794/616830)

- [How to install Git?](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

- [How to install NPM and NODE?](http://blog.teamtreehouse.com/install-node-js-npm-linux)

- [How to install Composer?](https://getcomposer.org/download/)

- [How to install MySQL?](https://www.howtoforge.com/tutorial/how-to-install-mysql-57-on-linux-centos-and-ubuntu/)



>PHP and additional packages for PHP and MySQL:


Debian:
 `sudo apt-get install -y mysql-community-server httpd php php-mysql php-fpm php-mbstring php-mcrypt php-xml php-xdebug php-curl`

Red Hat:
 `sudo yum install -y mysql-community-server httpd php php-mysql php-fpm php-mbstring php-mcrypt php-xml php-xdebug php-curl`



2: *Create database:*

`mysqladmin create replytest`



3: *Prepare project:*

`mkdir reply-project`

`cd reply-project`



4: *Clone reply-test repo:*

`git clone git@bitbucket.org:teamreply/reply-test.git`



5: *Create .env file inside reply-test dir and paste the following:*

`touch .env`

Paste this inside:
```
APP_NAME=local
APP_ENV=local
APP_KEY=base64:aDu7ZQMABE+4ET4eOdYb4g++OqKbyGsjCMaaFQbcsvo=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost:4000
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=replytest
DB_USERNAME=root
DB_PASSWORD=your_pass

TESTING_DB_CONNECTION=mysql
TESTING_DB_HOST=127.0.0.1
TESTING_DB_PORT=3306
TESTING_DB_DATABASE=replytest_functional
TESTING_DB_USERNAME=root
TESTING_DB_PASSWORD=your_pass

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=70c4cf61d49a7f
MAIL_PASSWORD=e5bf44fc4116f8
MAIL_ENCRYPTION=null

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=


DEBUGBAR_ENABLED=false
```


6: *Install backend assets (packages)*

`composer install`



7: *Make database migrations, seed and install OAuth2*

`php artisan migrate --seed && php artisan passport:install`



8: *Install frontend assets:*

`npm install`



9: *Build frontend:*

`npm run dev`



10: *Serve application with PHP inner server:*

`php artisan serve`



Go to _http://localhost:8000_

Voila!

____

## iOS steps


Tutorials:

- [How to install Homebrew?](https://www.moncefbelyamani.com/how-to-install-xcode-homebrew-git-rvm-ruby-on-mac/)
- https://blog.frd.mn/install-nginx-php-fpm-mysql-and-phpmyadmin-on-os-x-mavericks-using-homebrew/
- https://dev.to/chiefoleka/how-to-setup-nginx-and-php71-with-fpm-on-mac-os-x-without-crying-4m8


>Installing MySQL:

`brew install mysql`

`export PATH=/usr/local/mysql/bin:$PATH`

`mysql_secure_installation`



>Installing PHP:

`brew install php71`

`export PATH=/usr/local/bin:$PATH`



>Install composer:

`brew install composer`
___


## Windows steps

_Comming soon!_
